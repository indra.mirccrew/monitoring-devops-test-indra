1. git clone https://gitlab.com/indra.mirccrew/monitoring-devops-test-indra.git
2. docker-compose up -d

how to finish the task:
1. create docker compose file to deploy multi container, prometheus, grafana, cadvisor, node-exporter
2. node exporter for resource system host
3. cadvisor for resource docker container
4. add node exporter and cadvisor in prometheus.yml so prometheus can read data that node exporter and cadvisor already collect
5. add data source from prometheus in grafana
6. import dashboard from official dashboard grafana website, dashboard name is "node exporter full" and "docker and system monitoring"
7. dashboard is already setup for monitoring host or container 
8. "node exporter full" is for monitoring host resource, cpu, memory, disk, network
9. "docker and system monitoring" is for monitoring resource container, cpu, memory, disk, network

how to test deployment:
screenshot is already attaached inside folder from email

problem encountered:
node exporter and cadvisor not showing graph in dashboard

how to solve the problem:
1. check if container node exporter or cadvisor is running or not, if yes
2. check if node exporter and cadvisor is up in menu status>target at prometheus, if yes 
3. make sure the dashboard is working, if not, try another dashboard
